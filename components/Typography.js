import capsize from 'capsize';

import { styled } from '../stitches.config.js';

const reset = { margin: 0 };

const BASELINE = 22;

const HEADING_CAP_HEIGHT = 2.5; // in baselines
const HEADING_LINE_GAP = 1; // in baselines

const HEADING2_CAP_HEIGHT = 1.5; // in baselines
const HEADING2_LINE_GAP = 1; // in baselines

// Roboto
const fontMetrics = {
  capHeight: 1456,
  ascent: 1900,
  descent: -500,
  lineGap: 0,
  unitsPerEm: 2048
};

const headingStyles = capsize({
  fontMetrics,
  capHeight: HEADING_CAP_HEIGHT * BASELINE,
  lineGap: HEADING_LINE_GAP * BASELINE
});

const headingStyles2 = capsize({
  fontMetrics,
  capHeight: HEADING2_CAP_HEIGHT * BASELINE,
  lineGap: HEADING2_LINE_GAP * BASELINE
});

export const Heading1 = styled('h1', {
  ...reset, // TODO these resets should be global!
  ...headingStyles,
  color: 'red',
  fontFamily: 'Roboto' // TODO connect to theme
});

export const Heading2 = styled('h1', {
  ...reset, // TODO these resets should be global!
  ...headingStyles2,
  color: 'cyan',
  fontFamily: 'Roboto' // TODO connect to theme
});
