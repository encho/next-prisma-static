import { PrismaClient } from '@prisma/client';
import capsize from 'capsize';

import Button from '../components/Button';
import { Heading1, Heading2 } from '../components/Typography';

export async function getStaticProps() {
  const prisma = new PrismaClient();
  const songs = await prisma.song.findMany({
    include: { artist: true }
  });

  return {
    props: {
      songs
    }
  };
}

export default ({ songs }) => (
  <div>
    <Heading1>Heading1</Heading1>
    <Heading2>
      Heading2 Heading2 Heading2 Heading2 Heading2 Heading2 Heading2 Heading2
      Heading2 Heading2 Heading2 Heading2 Heading2 Heading2
    </Heading2>
    <Button>Button</Button>
    <Heading2>Data from sql lite:</Heading2>
    <ul>
      {songs.map((song) => (
        <li key={song.id}>{song.name}</li>
      ))}
    </ul>
  </div>
);
